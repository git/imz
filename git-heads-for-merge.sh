#!/bin/sh
#
# (Extracted from git-pull.sh; thanks to Junio C Hamano for the direction.)
# 
# Prints out the fetched heads intended for being merged
# (which are stored in FETCH_HEAD).
#
# This script makes inspecting the potential merge and 
# then manually doing it each a simpler command:
# 
# git log $(git heads-for-merge) --not HEAD
# git merge $(git heads-for-merge)
#
# Or for commits without a common ancestor:
#
# for h in $(git heads-for-merge); do git merge "$h"; done
#
# This sequence of commands is an analog of "Fun with FETCH_HEAD" 
# (http://) for the case of many fetched heads.

# To set up GIT_DIR:
. git-sh-setup

sed -e '/	not-for-merge	/d' \
	-e 's/	.*//' "$GIT_DIR"/FETCH_HEAD | \
	tr '\012' ' '
